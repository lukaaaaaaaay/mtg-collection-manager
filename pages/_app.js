import '../styles/globals.css'
// import Layout from '../components/layout/layout'
import 'bulma/css/bulma.css'
import { SWRConfig } from 'swr'
import fetchJson from '../lib/fetchJson'
import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';

function MyApp({ Component, pageProps }) {

  const getLayout = Component.getLayout || ((page) => page)
  return (

    <SWRConfig
      value={{
        fetcher: fetchJson,
        onError: (err) => {
          console.error(err)
        },
      }}
    >
      {getLayout(
          <Component {...pageProps} />
      )}
    </SWRConfig>

  )
}

export default MyApp
