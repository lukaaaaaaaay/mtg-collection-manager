import React, { useState } from 'react'
import useUser from '../lib/useUser'
import fetchJson, { FetchError } from '../lib/fetchJson';
import LoginLayout from '../components/layout/loginLayout';

export default function Login() {
    const { mutateUser } = useUser({
        redirectTo: '/',
        redirectIfFound: true,
    });

    const [errorMessage, setErrorMsg] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const body = {
          email: event.currentTarget.email.value,
          password: event.currentTarget.password.value
        }

        try {
          mutateUser(
            await fetchJson('/api/login', {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify(body),
            })
          )
        } catch (error) {
            if (error instanceof FetchError) {
                setErrorMsg(error.data.message)
              } else {
                console.error('An unexpected error happened:', error)
              }
        }
    }

    return (
    <>
        <div className="box">
            <p className="content">
                Login below
            </p>
            <form onSubmit={handleSubmit}>
                <div className="field">
                    <p className="control has-icons-left has-icons-right">
                        <input className="input" type="email" placeholder="Email" name="email" />
                        <span className="icon is-small is-left">
                            <i className="fas fa-envelope"></i>
                        </span>
                        {/* <span class="icon is-small is-right">
                            <i class="fas fa-check"></i>
                        </span> */}
                    </p>
                </div>
                <div class="field">
                    <p class="control has-icons-left">
                        <input className="input" type="password" placeholder="Password" name="password"/>
                        <span className="icon is-small is-left">
                        <i className="fas fa-lock"></i>
                        </span>
                    </p>
                </div>
                <div className="field">
                    <p className="control">
                        <button className="button is-default" type="submit">
                        Login
                        </button>
                    </p>
                </div>

                {errorMessage && 
                    <article className="message is-danger">
                        <div className="message-body">
                        {errorMessage}
                        </div>
                    </article>

                    }
            </form>
        </div>
    </>
  )
}

Login.getLayout = function getLayout(page) {
    return (
        <LoginLayout>
            {page}
        </LoginLayout>
    )
        
}