import Layout from "../components/layout/layout";

export default function CardLists() {
    return (
        <div>
            A list of CardLists
        </div>
    );

}

CardLists.getLayout = function getLayout(page) {
    return (
        <Layout>
            {page}
        </Layout>
    )
}