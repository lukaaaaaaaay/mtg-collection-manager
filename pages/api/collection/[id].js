import PocketBase from 'pocketbase'
import { withIronSessionApiRoute } from 'iron-session/next'
import { sessionOptions } from '../../../lib/session'

const client = new PocketBase('http://127.0.0.1:8090');
const removeOperationId = '2';

async function addToCollectionRoute(req, res) {

    switch(req.method) {
        case 'POST':
            const user = req.session.user.profile;
            if(user == null) {
                res.status(401).json('User not found. Please login');
            }
            const collectionId = req.body.collectionId;

            const existingCollection = await client.records.getOne('collection', collectionId);
            const existingCards = existingCollection.cards == null ? [] : existingCollection.cards;
            let data = {
                id: collectionId,
                owner: user.userId
            };
            
            if(req.body.operation === removeOperationId) {
                const idsToRemove = req.body.cards.map(card => card.id);
                const remainingCards = existingCards.filter(card => !idsToRemove.includes(card.id));
                data.cards = remainingCards;
            }
            else {
                data.cards = existingCards.concat(req.body.cards);
            }

            let record = await client.records.update('collection', collectionId, data);
            
            res.status(200).json(record);
            break;
        default:
            res.status(405).send({ message: 'Only POST requests allowed' });
    }
}

export default withIronSessionApiRoute(addToCollectionRoute, sessionOptions)

async function post(req, res) {
    
}