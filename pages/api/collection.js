import PocketBase from 'pocketbase'
import { withIronSessionApiRoute } from 'iron-session/next'
import { sessionOptions } from '../../lib/session'

const client = new PocketBase('http://127.0.0.1:8090');

 async function collectionRoute(req, res) {
    const user = req.session.user.profile;

    if(user == null) {
        res.status(401).json('User not found. Please login');
    }
   
    let collectionList = await client.records.getList('collection', 1, 50, {
        filter: `owner = "${user.userId}"`,
    });

    let collection = collectionList.items[0];

    if(collectionList.totalItems === 0) {
        collection = await client.records.create('collection', {
            owner: user.userId
        });
    }


    res.status(200).json(collection);
}

export default withIronSessionApiRoute(collectionRoute, sessionOptions)
