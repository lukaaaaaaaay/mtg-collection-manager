// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default async function handler(req, res) {
    
    const cardData = await fetch('https://api.scryfall.com/cards/search?q=' + req.query.q)
        .then((response) => response.text())
        .then(result => JSON.parse(result));
    res.status(200).json(cardData)
  }
  