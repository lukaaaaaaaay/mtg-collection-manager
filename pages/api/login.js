import { withIronSessionApiRoute } from 'iron-session/next'
import { sessionOptions } from '../../lib/session'
import PocketBase from 'pocketbase'

const client = new PocketBase('http://127.0.0.1:8090');

async function loginRoute(req, res) {
    
    const { email, password } = await req.body
  
    try {
      const authData = await client.users.authViaEmail(email, password);
  
      const user = { isLoggedIn: true, token: authData.token, profile: authData.user.profile }
      req.session.user = user
      await req.session.save()
      res.json(user)
    } catch (error) {
      res.status(500).json({ message: error.message })
    }
  }
  
  export default withIronSessionApiRoute(loginRoute, sessionOptions)