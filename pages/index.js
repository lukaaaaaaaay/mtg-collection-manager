import SimpleSearch from "../components/search/search";
import { useState } from "react";
import CardList from "../components/card/cardList";
import useUser from "../lib/useUser";
import Layout from "../components/layout/layout";
import Collection from "../components/collection/collection"


export default function Home() {
  
  const { user } = useUser({
    redirectTo: '/login',
  })

  const [cardData, setCardData] = useState([]);

  const handleSearchResult = data => {
    if(data != null) {
      setCardData(data.data);
    }
    
  };

  return (
    <div>
      <section className="hero">
        <div className="hero-body">
          <p className="title">
            MTG Collection Manager
          </p>
          {user && user.profile && user.profile.name && 
            <p className="subtitle">
              Put cards in me, {user.profile.name} 
            </p>
          }
        </div>
      </section>
      <Collection /> 
      <section className="section">

        <div>
          <CardList cards={cardData} />
        </div>
          
      </section>
    </div>
  )
}

Home.getLayout = function getLayout(page) {
  return (
      <Layout>
          {page}
      </Layout>
  )
      
}