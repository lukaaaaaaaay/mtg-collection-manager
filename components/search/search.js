
function SimpleSearch({handleSearch}) {


    return (
        <form name="simpleSearch" className="flex" onSubmit={handleSearch}>
            <div className="field has-addons">
                <div className="control is-expanded">
                    <input type="search" name="query" placeholder="Enter card name.."   className="input"></input>
                </div>
                <div className="control">
                    <button type="click" className="button is-default">Search</button>
                </div>
                
            </div>
        </form>
    )
}

export default SimpleSearch;