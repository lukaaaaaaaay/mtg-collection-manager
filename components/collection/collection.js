import { useState, useEffect } from "react";
import CardList from "../card/cardList";
import CardSearch from "../card/cardSearch";
import fetchJson, { FetchError } from '../../lib/fetchJson';

function Collection() {
    const [collection, setCollection] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        loadCollection();
    }, []);

    const openSearch = () => {
        const element = document.getElementById('searchResults');
        element.classList.add('is-active');
        
    }

    async function loadCollection() {

        const collection = await fetchJson('http://localhost:3000/api/collection');
        setCollection(collection);
        setLoading(false);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        let cards = [];
        const qty = event.target.quantity.value;
        for(let i=0; i < qty; i++) {
            cards.push(JSON.parse(event.target.card.value));
        }
        const body = {
            collectionId: collection.id,
            cards: cards,
            operation: event.target.operation.value
        };

        updateCollection(collection.id, body);

    }

    async function updateCollection(collectionId, body) {
        const result = await fetchJson(`http://localhost:3000/api/collection/${collectionId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        });
        loadCollection();
    }

    const addOperations = {
        submitHandler: handleSubmit,
        actions: [
            {
                id:1, 
                value: "Add"
            }
        ]
    };

    const removeOperations = {
        submitHandler: handleSubmit,
        actions: [
            {
                id: 2,
                value: "Remove"
            }
        ]
    }

    return (
        <section className="section">

            
            <div className="columns">
            {collection != null && collection.cards == null && 
                <div className="column is-half is-offset-one-quarter">
                    <div className="box">
                        <p>
                            Bummer! You don&apos;t have any cards in your collection
                        </p>
                        <p>
                            <button type="button" className="button is-primary" onClick={openSearch}>
                                Add some!
                            </button>
                        </p>
                    </div>
                    
                </div>
            }
            {collection != null && collection.cards != null && 
                <div className="column is-full">
                    <div>
                        <h2 className="subtitle">Cards in collection ({collection.cards.length})</h2>
                        <p>
                            <button type="button" className="button is-primary" onClick={openSearch}>
                                Add more!
                            </button>
                        </p>
                    </div>
                    <CardList cards={collection.cards} operations={removeOperations} />
                </div>
                
            }
            </div>
            
            <CardSearch operations={addOperations}/>
            
        </section>
    )
}

export default Collection;