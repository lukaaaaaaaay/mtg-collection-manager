import '../styles/Loader.module.scss'
export default function Loader() {
    return (
        <div className="loaderWrapper">
            <div className="loader is-loading"></div>
        </div>
    )
}