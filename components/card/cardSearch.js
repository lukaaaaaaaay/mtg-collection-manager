import SimpleSearch from "../search/search";
import CardList from "./cardList";
import {useState} from "react";
import styles from './Card.module.scss'
import Loader from "../loader";
import fetchJson from "../../lib/fetchJson";

function CardSearch({operations}) {
    const [cardData, setCardData] = useState([]);
    const [loading, setLoading] = useState(false);
    const handleSearchResult = (event) => {
        event.preventDefault();
        setLoading(true);
        performSearch(event.target[0].value);
    }

    const performSearch = async query => {
        const queryParams = new URLSearchParams({
            q: query,
        });
        const results = await fetchJson('/api/search?' + queryParams);

        if(results != null) {
            setCardData(results.data);
            setLoading(false);
        }
    };

    const close = () => {
        setCardData([]);
        var element = document.getElementById('searchResults');
        element.classList.remove('is-active');
    };

    return (
        <div id="searchResults" className="modal">
            <div className="modal-background"></div>
            
                <div className={styles.resultsContainer}>

                {/* {loading && } */}
      
                    <div className="content">
                        
                        <div className="block">
                            <SimpleSearch handleSearch={handleSearchResult}/>
                        </div>
                        {loading && 
                            <Loader/>
                        }
                        {!loading && 
                            <div className="block">
                                <CardList cards={cardData} operations={operations}></CardList>
                            </div>
                        }
                    </div>
                          
                </div>
     
            
            
            <button className="modal-close is-large" aria-label="close" onClick={close}></button>
        </div>
    )
}

export default CardSearch;