
function CardOperation({onSubmitHandler, card, operation}) {
    const validateQuantity = (event) => {
        const value = event.target.value;
        const errorMessage = document.getElementById('quantityErrorMessage');
        if(value != null && value !== '' ) {
            if(!isNumeric(value) || value < 1) {
                toggleErrors(errorMessage, event.target, true);
            }
            else {
                toggleErrors(errorMessage, event.target, false);
            }
        }
        else {
            toggleErrors(errorMessage, event.target, false);
        }
    }

    const toggleErrors = (errorElement, targetElement, isError) => {
        if(isError) {
            errorElement.classList.remove('is-hidden');
            targetElement.classList.add('is-danger');
        }
        else {
            errorElement.classList.add('is-hidden');
            targetElement.classList.remove('is-danger');
        }
        
    }

    const isNumeric = (value) => /^-?\d+$/.test(value);

    return (
        <form name="cardOperationForm" onSubmit={onSubmitHandler}>
            <input type="hidden" defaultValue={JSON.stringify(card)} name="card"/>
            <input type="hidden" defaultValue={operation.id} name="operation"/>
            <div className="field has-addons">
                <div className="control">
                    <input type="text" className="input is-small" onChange={validateQuantity} name="quantity"/> 

                    <p class="help is-danger is-hidden" id="quantityErrorMessage">Please enter a valid positive number</p>
                </div>
                <div className="control">
                    <button className="button  is-primary is-small" type="submit">{operation.value}</button>
                </div>
            </div>
            
        </form>
    )
}

export default CardOperation