import Card from "./card";

function CardList({cards, operations}) {

    const cardItems = cards.map((card) => 
        <Card card={card} key={card.id} operations={operations}/>
    );
    return ( 
        <div className="columns is-multiline">
            {cardItems}
        </div>
    )
}

export default CardList;