import Image from "next/image";
import CardOperation from "./cardOperation";

function Card({card, operations}) {
    return (
        <div className="column is-one-fifth">
            <div>
                <Image
                    src={card.image_uris.normal} 
                    alt={card.name} 
                    width={375}
                    height={525}
                    layout="responsive"
                    />

                {operations.actions.map((action) => 
                    <CardOperation key={action.id} onSubmitHandler={operations.submitHandler} card={card} operation={action}/>
                )}
            </div>
        </div>
    )
}

export default Card;