import NavBar from "./navbar";
import Head from "next/head";
export default function Layout({children}) {
    return (
      <>
        <Head>
          <title>MTG Collection Manager</title>
          <meta name="description" content="A way to manage your MTG collection" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>
        <header >
              <NavBar />
        </header>

          <main className="container">{children}</main>

        <footer>
            yep a footer
          </footer>
      </>
    )
  }