import Link from "next/link";
import useUser from '../../lib/useUser'
import { useRouter } from 'next/router'
import fetchJson from '../../lib/fetchJson'

function NavBar(props) {
    const { user, mutateUser } = useUser()
    const router = useRouter()
    const logout = async (event) => {
        event.preventDefault()
        mutateUser(
          await fetchJson('/api/logout', { method: 'POST' }),
          false
        )
        router.push('/login')
    }

    return (
        <nav className="navbar is-light">
            <div className="navbar-brand">
                <Link href="/">
                    <a  className="navbar-item">MTG Collection Manager</a>
                </Link>
            </div>
            <div className="navbar-menu">
                <div className="navbar-start">
                    <Link href="/card-lists">
                        <a className="navbar-link">Card Lists</a>
                    </Link>
                </div>
            </div>

            <div className="navbar-end">
                {user && user.isLoggedIn && 
                    <div className="navbar-item">
                        Hi, {user.profile.name}
                    </div>
                }
                <div className="navbar-item">
                    <div className="buttons">
                        <button className="button is-primary" type="button" onClick={logout}>
                            Logout
                        </button>
                    </div>
                </div>
            </div>
        </nav>
    );
}

export default NavBar;