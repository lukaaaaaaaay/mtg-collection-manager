import NavBar from "./navbar";
import Head from "next/head";
export default function LoginLayout({children}) {
    return (
      <>
        <Head>
          <title>MTG Collection Manager</title>
          <meta name="description" content="A way to manage your MTG collection" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>

        <main className="container">
            <section className="hero ">
                <div className="hero-body ">
                    <p className="title">
                        MTG Collection Manager
                    </p>
                    <p className="subtitle">
                        A place to manage your collection   
                    </p>
                </div>
            </section>
            <section>
                <div className="columns is-flex is-vcentered">
                    <div className="column">
                    {children}
                    </div>
            
                </div>
            </section>
        </main>
        


        <footer class="footer">
            <div class="content has-text-centered">
                <p>
                    <strong>MTG Collection Manager</strong> is a personal project of Luke Phelan to manage MTG Cards.
                </p>
            </div>
        </footer>
      </>
    )
  }